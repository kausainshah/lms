
from scipy.spatial import distance as dist
from imutils.video import VideoStream
from imutils import face_utils
# from threading import Thread
import numpy as np
# import pyglet
# import argparse
import imutils
import time
import dlib
import cv2

dlib.DLIB_USE_CUDA = True

predictor = dlib.shape_predictor("./fatigue_detection/shape_predictor_68_face_landmarks.dat")

# define two constants, one for the eye aspect ratio to indicate
# blink and then a second constant for the number of consecutive
# frames the eye must be below the threshold for to set off the
# alarm
EYE_AR_THRESH = 0.25
# EYE_AR_CONSEC_FRAMES = 48
EYE_AR_CONSEC_FRAMES = 4

'''
- EYE_AR_THRESH = 0.2 seems best for eye opening 
- EYE_AR_CONSEC_FRAMES = 20 : Number of consecutive frames for drowsiness
'''

# initialize the frame counter as well as a boolean used to
# indicate if the alarm is going off
COUNTER = 0
ALARM_ON = False

# grab the indexes of the facial landmarks for the left and
# right eye, respectively
(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]


# ap = argparse.ArgumentParser()
# ap.add_argument("-a", "--alarm", type=str, default="./alarm.wav",
#                 help="alarm file mp3/wav type")
# args = vars(ap.parse_args())


def sound_alarm(path):
	# play an alarm sound
	music = pyglet.resource.media('alarm.wav')
	music.play()
	pyglet.app.run()


def eye_aspect_ratio(eye):
	# compute the euclidean distances between the two sets of
	# vertical eye landmarks (x, y)-coordinates
	A = dist.euclidean(eye[1], eye[5])
	B = dist.euclidean(eye[2], eye[4])

	# compute the euclidean distance between the horizon
	# eye landmark (x, y)-coordinates
	C = dist.euclidean(eye[0], eye[3])

	# compute the eye aspect ratio
	ear = (A + B) / (2.0 * C)

	# return the eye aspect ratio
	return ear



def detect_drowsiness(frame, rect):
	global COUNTER
	global ALARM_ON

	drowsiness = False

	# frame = frame_orig.copy()

	gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	shape = predictor(gray_frame, rect)
	shape = face_utils.shape_to_np(shape)

	# extract the left and right eye coordinates, then use the
	# coordinates to compute the eye aspect ratio for both eyes
	leftEye = shape[lStart:lEnd]
	rightEye = shape[rStart:rEnd]
	leftEAR = eye_aspect_ratio(leftEye)
	rightEAR = eye_aspect_ratio(rightEye)

	# average the eye aspect ratio together for both eyes
	ear = (leftEAR + rightEAR) / 2.0

	# compute the convex hull for the left and right eye, then
	# visualize each of the eyes
	leftEyeHull = cv2.convexHull(leftEye)
	rightEyeHull = cv2.convexHull(rightEye)
	cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
	cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

	# check to see if the eye aspect ratio is below the blink
	# threshold, and if so, increment the blink frame counter
	if ear < EYE_AR_THRESH:
		COUNTER += 1

		# if the eyes were closed for a sufficient number of
		# then sound the alarm
		if COUNTER >= EYE_AR_CONSEC_FRAMES:

			cv2.putText(frame, "DROWSINESS ALERT!", (10, 30),
						cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

			drowsiness = True

			# # if the alarm is not on, turn it on
			# if not ALARM_ON:
			# 	ALARM_ON = True

			# 	# check to see if an alarm file was supplied,
			# 	# and if so, start a thread to have the alarm
			# 	# sound played in the background
			# 	if args["alarm"] != "":
			# 		t = Thread(target=sound_alarm,
			# 					args=(args["alarm"],))
			# 		t.deamon = True
			# 		t.start()

			# # draw an alarm on the frame
			# cv2.putText(frame, "DROWSINESS ALERT!", (10, 30),
			# 			cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

	# otherwise, the eye aspect ratio is not below the blink
	# threshold, so reset the counter and alarm
	else:
		COUNTER = 0
		ALARM_ON = False

	# draw the computed eye aspect ratio on the frame to help
	# with debugging and setting the correct eye aspect ratio
	# thresholds and frame counters
	cv2.putText(frame, "EAR: {:.2f}".format(ear), (300, 30),
				cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

	return frame, drowsiness 
