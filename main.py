import numpy as np
from utils import *
from scipy.spatial.distance import cdist
import os
from create_db import generate_database

similaty_threshold = 25

def get_id(curr_embedding, embeddings, db):
	metric = "euclidean"
	dist = cdist(curr_embedding[np.newaxis, :], embeddings, metric=metric)
	min_idx = np.argmin(dist)

	# import pdb; pdb.set_trace()
	
	if dist[:, min_idx][0] > similaty_threshold:
		return "OTHER"
	# print("Min Dist", dist)

	return db[min_idx]

def main():
	# Generate database
	generate_database()

	# Load numpy embeddings
	embeddings = np.load("./database/embeddings.npy")
	db = np.load("./database/db.npy", allow_pickle=True)

	fd = FaceDetector()

	video_path = "./em.m4v" 
	cap = cv2.VideoCapture(0)

	output_path = os.path.join("output/recog_demo_new_1.mp4")
	fps = cap.get(cv2.CAP_PROP_FPS)
	# fps = 12
	frame_size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
	output_stream = cv2.VideoWriter(output_path, cv2.VideoWriter.fourcc(*'MJPG'), fps, frame_size)

	while cap.isOpened():
		try:
			ret, image = cap.read()
			output_image  = image.copy()

			predictions = fd.forward(output_image)

			for pred in predictions:
				left, top, right, bottom = list(map(int, pred.bbox))

				curr_embedding = pred.embedding
				# import pdb; pdb.set_trace()
				name_of_person = get_id(curr_embedding, embeddings, db.item())

				# Create box around face
				cv2.rectangle(output_image, (left, top), (right, bottom), (255, 0, 0), 2)

				# Write age and gender on the box
				cv2.putText(output_image, f"{name_of_person}", (left, top), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), thickness=2)
			
			output_stream.write(output_image)
			cv2.imshow("output", output_image)
			key = cv2.waitKey(1)
			
			if key == ord('q'):
				break
		except KeyboardInterrupt:
			break
	
	cv2.destroyAllWindows()
	cap.release()
	output_stream.release()

if __name__ == "__main__":
	main()