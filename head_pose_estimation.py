import numpy as np
from numpy import clip
from ie_module import Module
from utils import resize_input
import cv2
from openvino.inference_engine import IENetwork
from ie_module import InferenceContext
from platform import system
class TestDetector(Module):

    class Results():
        def __init__(self, outputs):
            self.outputs = outputs
    def __init__(self, model):
        super(TestDetector, self).__init__(model)
        self.input_blob = next(iter(model.inputs))
        self.output_blob = next(iter(model.outputs))
        self.input_shape = model.inputs[self.input_blob].shape
        self.output_shape = model.outputs[self.output_blob].shape
    def preprocess(self, frame):
        assert len(frame.shape) == 4, "Frame shape should be [1, c, h, w]"
        assert frame.shape[0] == 1
        assert frame.shape[1] == 3
        input = resize_input(frame, self.input_shape)
        return input
    def start_async(self, frame):
        input = self.preprocess(frame)
        self.enqueue(input)
    def enqueue(self, input):
        return super(TestDetector, self).enqueue({self.input_blob: input})
    def get_attributes(self, frame):
        outputs = self.get_outputs()[0]
        # import pdb; pdb.set_trace()
        # outputs = np.squeeze(outputs)
        # results = TestDetector.Results(outputs)
        return outputs

import glob
import os.path as osp
import logging as log

def load_model(model_path):
        model_path = osp.abspath(model_path)
        model_description_path = model_path
        model_weights_path = osp.splitext(model_path)[0] + ".bin"
        log.info("Loading the model from '%s'" % (model_description_path))
        assert osp.isfile(model_description_path), \
            "Model description is not found at '%s'" % (model_description_path)
        assert osp.isfile(model_weights_path), \
            "Model weights are not found at '%s'" % (model_weights_path)
        model = IENetwork(model_description_path, model_weights_path)
        log.info("Model is loaded")
        return model

# DEFINE THESE FIRST <==================
# CROPS_DIRECTORY = "./test_crops/*.png"

OS = system()
CPU_LIB = ''
if 'Linux' in OS:
    # For Ubuntu
    CPU_LIB = "/opt/intel/openvino/deployment_tools/inference_engine/lib/intel64/libcpu_extension_avx2.so"
else:
    # For MacOS Use below extension
    CPU_LIB = "/opt/intel/openvino/deployment_tools/inference_engine/lib/intel64/libcpu_extension.dylib"


XML_FILE = "./models/head-pose-estimation-adas-0001.xml"

class HeadPoseEstimation():

    Pitch_Threshold_Range = 10
    Yaw_Threshold_Range = 10
    Roll_Threshold_Range = 10

    def __init__(self):
        self.model = load_model(XML_FILE)
        used_devices = set(["CPU"])
        context = InferenceContext()
        context.load_plugins(used_devices, CPU_LIB, "")
        for d in used_devices:
            context.get_plugin(d).set_config({
                "PERF_COUNT": "YES"})

        self.person_attr = TestDetector(self.model)
        self.person_attr.deploy("CPU", context)
        log.info("Person Detector is loaded")

        self.curr_pitch = 0 
        self.curr_roll = 0
        self.curr_yaw = 0

    def estimate(self, frame):
        original_frame = frame.copy()
        frame = frame.transpose((2, 0, 1)) # HWC to CHW
        frame = np.expand_dims(frame, axis=0)
        
        self.person_attr.clear()
        self.person_attr.start_async(frame)
        results = self.person_attr.get_attributes(frame)
        # DO PROCESSING ON OUTPUT HERE <=============
        # cv2.imshow("Vis", results)      

        # import pdb; pdb.set_trace()
        # for pose in results:
        self.curr_pitch = results['angle_p_fc'][0][0]
        self.curr_roll = results['angle_r_fc'][0][0]
        self.curr_yaw = results['angle_y_fc'][0][0]            
        return 
            

    def compute_head_pose_anomaly(self):
        if self.curr_pitch > self.Pitch_Threshold_Range or self.curr_pitch < -self.Pitch_Threshold_Range:
            return True
        if self.curr_yaw > self.Yaw_Threshold_Range or self.curr_yaw < -self.Yaw_Threshold_Range:
            return True
        if self.curr_roll > self.Roll_Threshold_Range or self.curr_roll < -self.Roll_Threshold_Range:
            return True
        
        return False




if __name__ == "__main__":
    main()





