import time
import cv2
import numpy as np
# from lms_demo import draw_text_with_background


def draw_text_with_background(frame, text, origin,
                              font=cv2.FONT_HERSHEY_SIMPLEX, scale=1.0,
                              color=(0, 0, 0), thickness=1, bgcolor=(255, 255, 255)):
    text_size, baseline = cv2.getTextSize(text, font, scale, thickness)
    # import pdbß
    # pdb.set_trace()
    cv2.rectangle(frame,
                  tuple((origin + (0, baseline)).astype(int)),
                  tuple((origin + (text_size[0], -text_size[1])).astype(int)),
                  bgcolor, cv2.FILLED)
    cv2.putText(frame, text,
                tuple(origin.astype(int)),
                font, scale, color, thickness)
    return text_size, baseline


class Time_Module():

    # present_time = 0
    # absent_time = 0
    # fatigue_time = 0
    # start_time = ""

    state_counter = {'present_time': 0, 'absent_time': 0, "fatigue_time": 0}
    # previous_state_content = {'present_time': "0",
    #                           'absent_time': "0", "fatigue_time": "0"}

    previous_state = ""
    state = ""
    # state_timer = 0
    state_change = False


    initially_called = True

    intermediate_absent_false_occurance = False

    def __init__(self, fps=1):
        self.fps = fps

    # states
    # pp pa pf
    # ap aa af
    # fp fa ff

    def update_time(self, time_state, frame, frame_counter, models_inference_time):

        self.state = time_state

        if self.initially_called:
            self.previous_state = self.state
            self.initially_called = False
            return

        if self.state == "pt" and self.previous_state == "pt":
            # Working
            self.state_change = False
            self.state_counter["present_time"] += 1
        elif self.state == "pt" and self.previous_state == "at":
            # Working
            self.state_change = True
            pass
        elif self.state == "pt" and self.previous_state == "ft":
            # Working
            self.state_change = True
            pass
        elif self.state == "at" and self.previous_state == "pt":
            # Working
            self.state_change = True
            pass
        elif self.state == "at" and self.previous_state == "at":
            # Working
            self.state_change = False
            self.state_counter["absent_time"] += 0.6
        elif self.state == "at" and self.previous_state == "ft":
            # Working
            self.state_change = True
            pass
        elif self.state == "ft" and self.previous_state == "pt":
            # Working
            self.state_change = True
            pass
        elif self.state == "ft" and self.previous_state == "at":
            # Working
            self.state_change = True
            pass
        elif self.state == "ft" and self.previous_state == "ft":
            self.state_change = False
            self.state_counter["fatigue_time"] += 1

        self.previous_state = self.state

        return

    def state_changes(self):
        return self.state_change

    def frames2time(self, frame_count):
        time = frame_count / self.fps

        minutes = int(time % 60)
        seconds = int(time // 60)

        time_string = ""

        if seconds < 10:
            time_string += f"0{seconds}"
        else:
            time_string += str(seconds)

        time_string += ":"

        if minutes < 10:
            time_string += f"0{minutes}"
        else:
            time_string += str(minutes)

        return time_string

    def show_time(self, frame):
        height, width, _ = frame.shape

        # pt_time = self.H_M_S_format(float(self.content['present_time']))
        # pt_time = str(round(float(
        #     self.content['present_time']) + float(self.previous_state_content['present_time']), 2))
        pt_time = self.frames2time(self.state_counter["present_time"])
        draw_text_with_background(
            frame, "PT = " + str(pt_time), np.array((0, height-90)))

        # at_time = self.H_M_S_format(float(self.content['absent_time']))

        # at_time = str(round(float(
        #     self.content['absent_time']) + float(self.previous_state_content['absent_time']), 2))
        at_time = self.frames2time(self.state_counter["absent_time"])
        draw_text_with_background(
            frame, "AT = " + str(at_time), np.array((0, height-60)))

        # ft_time = self.H_M_S_format(float(self.content['fatigue_time']))
        # ft_time = str(round(float(
        #     self.content['fatigue_time']) + float(self.previous_state_content['fatigue_time']), 2))
        ft_time = self.frames2time(self.state_counter["fatigue_time"])
        draw_text_with_background(
            frame, "FT = " + str(ft_time), np.array((0, height - 30)))

        return frame

    # def add_times(self, t1, t2):
    # 	# import pdb; pdb.set_trace()

    # 	if t1 and t2:
    # 		return float(t1) + float(t2)

    # 	elif t1=='' and t2:
    # 		return t2

    # def H_M_S_format(self, seconds):
    # 	hours = seconds // 3600
    # 	minutes = (seconds % 3600) // 60
    # 	sec = seconds % 60
    # 	content = str(hours) + ":" + str(minutes) + ":" + str(sec)
    # 	return content
