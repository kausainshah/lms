# LMS

## About
- Involves Facial Detection and Recognition from Insightface
- Involves Fatigue Detection (Drowsiness) from Dlib eyelid shape Prediction

## Use Case
- conda env create -f environment.yml   
``` or ```
- pip install -r requirements.txt   
- For installing without pip errors
`cat requirements.txt | xargs -n 1 pip install`

`pip install insightface==0.1.5 --pre`

* For MacOS change CPU_LIB path from head_pose_estimation.py *

` python lms_demo.py --video_path path_to_vid`


## Info:
- person_to_track : The person to be tracked or searched according to which the    
couter will operate   
` Can be NULL and the Initial Face with Maximum area is considered to track`  
` Can be name of someone from database and that person will be trakced `

## How it Works:
- Firstly the Face Embeddings for each face in __./database/images"__ is computed and    
saved as __embeddings.npy__ along with __db.npy__ with names of persons regarding each embedding 
- Than for each Face Detected Facial Features are computed and matched with database based on euclidean distance
- Based on the __person_to_track defined in Info__ variable the desired person is considered for the eyelib landmarks based on which and certain threshold checks it is considered drowsy  

## Thresholding:
- EYE_AR_THRESH = 0.2 seems best for eye opening 
- EYE_AR_CONSEC_FRAMES = 20 : Number of consecutive frames for drowsiness
- SIMILARITY_THRESHOLD : values greater than this are considered OTHER category of 
facial recognition, as it acts like similarity and greater than this means none of 
that face is registered in database 
- ABSENT_FRAMES_THRESHOLD : Number of consistent frames after which the person
to be tracked is considered absent

## Improvements:
- generic output dimension for video writing
- auto fps computation for video writing
- compute drowsiness only for specific person i.e the person to track

## Demo:
- person to be tracked __ceo__  with __EYE_AR_THRESH = 0.3__  
![](gif.gif)