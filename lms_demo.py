import time
import argparse
from time_module import Time_Module
import imutils
import dlib
from fatigue_detection.drowsiness_utils import detect_drowsiness
from create_db import generate_database
import os
from scipy.spatial.distance import cdist
from utils import *
import cv2
import numpy as np
import math
from head_pose_estimation import HeadPoseEstimation
'USE:'
'python lms_demo.py --video_path path_to_vid'


# tweakable parameters
'''
SIMILARITY_THRESHOLD : values greater than this are considered OTHER category of
facial recognition, as it acts like similarity and greater than this means none of
that face is registered in database
'''
SIMILARITY_THRESHOLD = 25

'''
ABSENT_FRAMES_THRESHOLD : Number of consistent frames after which the person
to be tracked is considered absent
'''
ABSENT_FRAMES_THRESHOLD = 4

'''
HEAD_POSE_ANOMALY_FRAMES_THRESHOLD : Number of consistent frames after which fatigue time will increase
as the person's head pose (person to be tracked) involves an Anaomaly  
'''
HEAD_POSE_ANOMALY_FRAMES_THRESHOLD = 4


REGISTRY_FRAMES_THRESHOLD = 3


ap = argparse.ArgumentParser()
ap.add_argument("--video_path", type=str, default=0)
ap.add_argument("--GPU", type=int, default=-1)
args = vars(ap.parse_args())


def draw_text_with_background(frame, text, origin,
                              font=cv2.FONT_HERSHEY_SIMPLEX, scale=1.0,
                              color=(0, 0, 0), thickness=1, bgcolor=(255, 255, 255)):
    text_size, baseline = cv2.getTextSize(text, font, scale, thickness)
    cv2.rectangle(frame,
                  tuple((origin + (0, baseline))),
                  tuple((origin + (text_size[0], -text_size[1]))),
                  bgcolor, cv2.FILLED)
    cv2.putText(frame, text,
                tuple(origin),
                font, scale, color, thickness)
    return text_size, baseline


def convert(seconds):
    min, sec = divmod(seconds, 60)
    hour, min = divmod(min, 60)
    return "%d:%02d:%02d" % (hour, min, sec)


def get_id(curr_embedding, embeddings, db):
    metric = "euclidean"
    dist = cdist(curr_embedding[np.newaxis, :], embeddings, metric=metric)
    min_idx = np.argmin(dist)

    # import pdb; pdb.set_trace()

    if dist[:, min_idx][0] > SIMILARITY_THRESHOLD:
        return "OTHER"
    # print("Min Dist", dist)

    return db[min_idx]


# def face_crop_in_gray(frame, left, top, right, bottom):
#     # import pdb; pdb.set_trace()
#     crop = frame[top:bottom, left:right]

#     # cv2.imshow("crop", crop)
#     # cv2.waitKey(0)
#     # cv2.destroyAllWindows()

#     return crop


def calc_area(left, top, right, bottom):
    w = right - left
    l = bottom - top
    area = l * w
    return area


def biggest_face_prediction(predictions):
    area_list = []
    for pred in predictions:
        left, top, right, bottom = list(map(int, pred.bbox))
        area_list.append(calc_area(left, top, right, bottom))

    max_area = max(area_list)
    index = area_list.index(max_area)
    predictions_new = predictions[index]
    # import pdb; pdb.set_trace()
    return [predictions_new]

def imcrop(img, x1, y1, x2, y2):
    # x1, y1, x2, y2 = bbox
    if x1 < 0 or y1 < 0 or x2 > img.shape[1] or y2 > img.shape[0]:
            img, x1, x2, y1, y2 = pad_img_to_fit_bbox(img, x1, x2, y1, y2)
    return img[y1:y2, x1:x2, :]

def pad_img_to_fit_bbox(img, x1, x2, y1, y2):
    img = cv2.copyMakeBorder(img, - min(0, y1), max(y2 - img.shape[0], 0),
                            -min(0, x1), max(x2 - img.shape[1], 0),cv2.BORDER_REPLICATE)
    y2 += -min(0, y1)
    y1 += -min(0, y1)
    x2 += -min(0, x1)
    x1 += -min(0, x1)
    return img, x1, x2, y1, y2


def main():
    # Generate database
    generate_database()

    # Load numpy embeddings
    embeddings = np.load("./database/embeddings.npy")
    db = np.load("./database/db.npy", allow_pickle=True)

    fd = FaceDetector(ctx_id=args['GPU'])

    detector = dlib.get_frontal_face_detector()

    # video_path = "./inputs/1.mp4"
    cap = cv2.VideoCapture(args['video_path'])

    files = len(os.listdir('./output/')) + 1
    output_path = os.path.join(f"output/Demo_{files}.mp4")

    output_stream = ''

    previous_person = ""

    Time = None

    frame_counter = 0

    number_of_absent_frames = 0

    number_of_registered_frames = 0

    head_pose_anomaly_frames = 0

    '''
	person_to_track : The person to be tracked or searched according to which the
	couter will operate
	Can be NULL and the Initial Face with Maximum area is considered to track
	Can be name of someone from database and that person will be trakced
	'''
    person_to_track = ''
    person_face_registered = False
    # frameCounter = 0
    models_inference_time = 0
    name_of_person = ''
    while cap.isOpened():
        try:
            # mainTimeS = time.time()

            ret, image = cap.read()
            # ret, image = cap.read()

            # image = imutils.rotate(image, -90)
            if not ret:
                break
            output_image = imutils.resize(image.copy(), width=720)

            # cv2.putText(output_image, f"{frame_counter}", (50, 50),
            #             cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), thickness=2)

            frame_size = (output_image.shape[1], output_image.shape[0])

            if frame_counter == 0:
                # fps = cap.get(cv2.CAP_PROP_FPS)

                if args['video_path'] == 0:
                    fps = 1
                else:
                    fps = cap.get(cv2.CAP_PROP_FPS)
                    
                Time = Time_Module(fps=fps)

                print("#### FPS", fps)
                # frame_size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
                frame_size = (720, 405)
                output_stream = cv2.VideoWriter(
                    output_path, cv2.VideoWriter.fourcc(*'MJPG'), fps, frame_size)

            # print(frame_size)
            # output_image  = imutils.resize(image.copy())
            # t3 = time.time()
            predictions = fd.forward(output_image)
            # t4 = time.time()
            # face_emb_time = t4-t3
            # detect faces in the grayscale frame
            # rects = detector(gray, 0)

            # for pred in predictions:
            # 	import pdb; pdb.set_trace()

            no_of_faces = len(predictions)

            frame_counter += 1

            if no_of_faces > 1:
                # print("getting biggest face")
                predictions = biggest_face_prediction(predictions)

            # name_of_person = ''

                # Only biggest face in predictions
            for pred in predictions:
                # x1, y1, x2, y2 == Format 
                left, top, right, bottom = list(map(int, pred.bbox))

                # cv2.rectangle(output_image, (left, top), (right, bottom), (255,255,255, 1))
                # cv2.imshow("frame", output_image)
                # cv2.waitKey(0)
                # cv2.destroyAllWindows()
                # import pdb; pdb.set_trace()

                curr_embedding = pred.embedding
                # import pdb; pdb.set_trace()
                name_of_person = get_id(curr_embedding, embeddings, db.item())

                if name_of_person == person_to_track:
                    number_of_absent_frames = 0

                rect = dlib.rectangle(left=left, top=top,
                                      right=right, bottom=bottom)

                # image = imutils.resize(image, width=450)

                # Fatigue detection
                output_image, drowsiness = detect_drowsiness(
                    output_image, rect)

                # Create box around face
                cv2.rectangle(output_image, (left, top),
                              (right, bottom), (255, 0, 0), 2)

                # face_crop = face_crop_in_gray(image, left, top, right, bottom)

                ################################################################
                ###########         Head Pose Estimator             ############
                ################################################################
                head_pose_estimator = HeadPoseEstimation()
    
                croped_face = imcrop(output_image, left, top, right, bottom)

                # croped_face = output_image[top:bottom, left:right]
                # cv2.imshow("ww", croped_face)
                # cv2.waitKey(0)
                head_pose_estimator.estimate(croped_face)

                cv2.putText(output_image, f"YAW {head_pose_estimator.curr_yaw:.2f}", (50,50), cv2.FONT_HERSHEY_COMPLEX, 1, (0,0,255), 1)
                cv2.putText(output_image, f"pitch {head_pose_estimator.curr_pitch:.2f}", (50,80), cv2.FONT_HERSHEY_COMPLEX, 1, (255,0,0), 1)
                cv2.putText(output_image, f"ROLL {head_pose_estimator.curr_roll:.2f}", (50,110), cv2.FONT_HERSHEY_COMPLEX, 1, (0,255,0), 1)

                if previous_person:
                    if previous_person == person_to_track:
                        if drowsiness:
                            Time.update_time(
                                "ft", output_image, frame_counter, models_inference_time)
                        
                        if head_pose_estimator.compute_head_pose_anomaly():
                            head_pose_anomaly_frames += 1
                            if head_pose_anomaly_frames >= HEAD_POSE_ANOMALY_FRAMES_THRESHOLD:
                                Time.update_time(
                                    "ft", output_image, frame_counter, models_inference_time)
                                # head_pose_anomaly_frames = 0
                                # FIXME: 2 line below
                                if Time.state_change:
                                    head_pose_anomaly_frames = 0


                        elif name_of_person == person_to_track:
                            Time.update_time(
                                "pt", output_image, frame_counter, models_inference_time)
                    # elif previous_person != name_of_person:
                    #     Time.update_time("at", output_image,
                    #                      frame_counter, models_inference_time)

                # output_image = Time.show_time(output_image)

                # output_image = Time.update_time("pt", output_image)

                # a = time.time()

                # # Write age and gender on the box
                cv2.putText(output_image, f"{name_of_person}", (left, top),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), thickness=2)

                # print(name_of_person)

                # previous_person = name_of_person if name_of_person != "OTHER" else ""
                # if previous_person == name_of_person:
                #     number_of_registered_frames += 1
                # else:
                #     number_of_registered_frames == 0

                # if number_of_registered_frames >= REGISTRY_FRAMES_THRESHOLD:
                if not person_to_track:
                    if name_of_person == "OTHER":
                        pass
                    else:
                        if not person_face_registered:
                            person_to_track = name_of_person
                            person_face_registered = True
                            print(f"{name_of_person}'s Face Regtered")

                # elif person_to_track and frame_counter == 1:
                # 	print(f"{name_of_person}'s Face Already Regtered")

                # cv2.imshow("output", output_image)
                # cv2.waitKey(0)

                previous_person = name_of_person

                # Loop ENDS

            if (len(predictions) == 0) or name_of_person != person_to_track:
                # Increase Absent time
                # f number_of_absent_frames == 0:
                # p_time = Time.previous_state_content['present_time']

                if number_of_absent_frames >= 0:
                    # Time.previous_state_content['present_time'] = p_time
                    Time.intermediate_absent_false_occurance = True

                number_of_absent_frames += 1

                if number_of_absent_frames >= ABSENT_FRAMES_THRESHOLD:
                    Time.update_time("at", output_image,
                                     frame_counter, models_inference_time)

            # Show Timings on Frame
            frame_with_time = Time.show_time(output_image)

            # print(output_image.shape)
            output_stream.write(output_image)

            # output resized
            # output_image = cv2.resize(output_image, (512, 512))

            cv2.imshow("output", output_image)
            cv2.waitKey(1)

            # mainTimeE = time.time()

            # print(math.ceil(1/(math.ceil(float(mainTimeE-mainTimeS)))))
            # print(output_image.shape)
            # key = cv2.waitKey()

            # if key == ord('q') & 0xFF:
            #     break
        except KeyboardInterrupt:
            output_stream.release()
            cv2.destroyAllWindows()
            cap.release()
            exit()

    cv2.destroyAllWindows()
    cap.release()
    output_stream.release()
    print("Video End")


if __name__ == "__main__":
    main()


# TODO: 
# - add exception handling for
    # - setting thresholds realted to Head pose anomaly