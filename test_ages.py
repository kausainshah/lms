import argparse
import os
from utils import *

def test1():
    fd = FaceDetector()

    if opt.use_webcam:
        cap = cv2.VideoCapture(0)
    else:
        cap = cv2.VideoCapture(opt.input)
        
    output_path = os.path.join(opt.output_folder, opt.input)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    output_stream = cv2.VideoWriter(output_path, cv2.VideoWriter.fourcc(*'MJPG'), fps, frame_size)

    while cap.isOpened():
        try:
            ret, image = cap.read()
            output_image  = image.copy()

            predictions = fd.forward(output_image, inp_size = (320, 180))

            for pred in predictions:
                left, top, right, bottom = list(map(int, pred.bbox))
                gender = pred.gender
                age = pred.age

                # Create box around face
                cv2.rectangle(output_image, (left, top), (right, bottom), (255, 0, 0), 2)

                # Write age and gender on the box
                cv2.putText(output_image, f"{age}", (left, top), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), thickness=2)

            cv2.imshow("output", output_image)
            key = cv2.waitKey(1)

            
            output_stream.write(output_image)

            if key == ord('q'):
                break

        except KeyboardInterrupt:
            print("stopped writing frame")
            break

    cap.release()
    output_stream.release()
    cv2.destroyAllWindows()

    return

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, default='mall_vid.mp4', help='File path to the video to be processed')
    parser.add_argument('--output_folder', type=str, default='output/', help='File path to the video to be processed')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--use-webcam', action='store_true', help='display results')

    opt = parser.parse_args()

    test1()
