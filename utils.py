import insightface
import cv2
import numpy as np
from platform import system
def draw_text_with_background(frame, text, origin,
                                  font=cv2.FONT_HERSHEY_SIMPLEX, scale=1.0,
                                  color=(0, 0, 0), thickness=1, bgcolor=(255, 255, 255)):
        text_size, baseline = cv2.getTextSize(text, font, scale, thickness)
        cv2.rectangle(frame,
                      tuple((origin + (0, baseline))),
                      tuple((origin + (text_size[0], -text_size[1]))),
                      bgcolor, cv2.FILLED)
        cv2.putText(frame, text,
                    origin,
                    font, scale, color, thickness)
        return text_size, baseline

def resize_input(frame, target_shape):
    assert len(frame.shape) == len(target_shape), \
        "Expected a frame with %s dimensions, but got %s" % \
        (len(target_shape), len(frame.shape))
    assert frame.shape[0] == 1, "Only batch size 1 is supported"
    n, c, h, w = target_shape
    input = frame[0]
    if not np.array_equal(target_shape[-2:], frame.shape[-2:]):
        input = input.transpose((1, 2, 0)) # to HWC
        input = cv2.resize(input, (w, h))
        input = input.transpose((2, 0, 1)) # to CHW
    return input.reshape((n, c, h, w))

    
class FaceDetector():
    def __init__(self, ctx_id = 0, nms = 0.4):
        OS = system()
        if 'Linux' in OS:
            # ctx_id=0  -> GPU
            # cts_id=-1 -> CPU
            ctx_id = ctx_id
        else: 
            ctx_id = -1 #CPU
        self.model = insightface.app.FaceAnalysis()
        self.model.prepare(ctx_id = ctx_id, nms=0.4)
        

    def forward(self, image, inp_size = None):
        """Forwards throught the insight face model

        Args:
            image ([type]): [description]
            inp_size ([tuple], optional): [description]. Defaults to None.
        """
        
        input_image = image
        if inp_size:
            input_image = cv2.resize(image, inp_size)

        output = self.model.get(image)
        
        return output
        
        
        

