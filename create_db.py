from utils import *
import pickle
import glob

IMAGES_PATH = "./database/images"

def generate_database():
    #  Traverse all images in the
    fd = FaceDetector()
    
    embeddings = []
    db = {}

    for idx, ip in enumerate(glob.glob(IMAGES_PATH+"/*")):
        id = ip.split("/")[-1].split(".")[0]
        print(id)
        im = cv2.imread(ip)
        out = fd.forward(im)

        if len(out) > 0:
            # Take the first
            embeddings.append(out[0].embedding)

            print("Age: ", out[0].age)

            db[idx] = id
     
    embeddings = np.array(embeddings)

    # print(embeddings.shape)

    # Save embeddings and associated dictionary
    np.save("database/embeddings.npy", embeddings)
    np.save("database/db.npy", db)

    return

if __name__ == "__main__":
    generate_database()